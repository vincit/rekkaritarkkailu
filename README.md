# Rekkaritarkkailu

**rekkaritarkkailu.fi** is a website for tracking vehicle licence plate spotting progress.

To start the server:

0. Copy `config/*.exs.dist` to `config/*.exs` and set up correct values.
1. Copy `lib/rekkaritarkkailu/endpoint.ex.dist` to `lib/rekkaritarkkailu/endpoint.ex` and set up salts.
2. Install dependencies with `mix deps.get`
3. Migrate with `mix ecto.migrate`
4. Start Phoenix endpoint with `mix phoenix.server`

Now you can visit `localhost:4000` from your browser.
