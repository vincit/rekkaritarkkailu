defmodule Rekkaritarkkailu.Mixfile do
  use Mix.Project

  def project do
    [app: :rekkaritarkkailu,
     version: "0.0.1",
     elixir: "~> 1.0",
     elixirc_paths: ["lib", "web"],
     compilers: [:phoenix] ++ Mix.compilers,
     deps: deps]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [mod: {Rekkaritarkkailu, []},
     applications: [:phoenix, :cowboy, :postgrex, :ecto, :logger, :comeonin]]
  end

  # Specifies your project dependencies
  #
  # Type `mix help deps` for examples and options
  defp deps do
    [{:phoenix, "~> 0.10.0"},
     {:phoenix_ecto, "~> 0.1"},
     {:ecto, "~> 0.9.0"},
     {:postgrex, ">= 0.0.0"},
     {:cowboy, "~> 1.0"},
     {:comeonin, "~> 0.3.0"}]
  end
end
