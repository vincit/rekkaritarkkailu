defmodule Rekkaritarkkailu.AuthController do
  use Rekkaritarkkailu.Web, :controller
  alias Rekkaritarkkailu.AuthLib
  alias Rekkaritarkkailu.Flash

  @moduledoc """
  Controller for all authentication related stuff.
  """

  plug :action
  
  def register(conn, _params) do
    render conn, "register.html", title: "Rekisteröityminen"
  end

  def register_do(conn, params) do
    case AuthLib.create_user(params) do
      {:ok, new_user} ->
        conn
        # Authenticate the registered user
        |> AuthLib.authenticate(new_user)
        |> Flash.put(:success, "Tunnus luotu onnistuneesti!")
        |> redirect to: "/"
      
      {:error, e} ->
        case e do
          %Ecto.Changeset{errors: validation_errors} when is_list(validation_errors) ->
            handle_register_errors conn, validation_errors

          :missing_password ->
            conn
            |> Flash.put(:error, "Salasana puuttuu.")

          _ ->
            conn
            |> Flash.put(:error, "Tunnuksen luominen epäonnistui tuntemattoman virheen takia.")
        end
        |> redirect to: "/rekisterointi"
    end
  end

  
  def login(conn, _params) do
    render conn, "login.html", title: "Sisäänkirjautuminen"
  end

  def login_do(conn, params) do
    case AuthLib.login conn, Map.get(params, "username"), Map.get(params, "password") do
      {:ok, user} ->
        conn
        |> AuthLib.authenticate(user)
        |> redirect to: "/"

      :error ->
        conn
        |> Flash.put(:error, "Väärä käyttäjätunnus tai salasana.")
        |> redirect to: "/sisaan"
    end
  end


  def logout(conn, _params) do
    conn
    |> AuthLib.logout
    |> redirect to: "/"
  end


  defp handle_register_errors(conn, [error | rest]) do
    msg = case error do
      {:username, :unique} -> "Käyttäjätunnus on jo käytössä."
      {:username, {:too_short, _}} -> "Käyttäjätunnus puuttuu."
      {:username, {:too_long, _}} -> "Käyttäjätunnus on liian pitkä."
      {:email, {:too_short, _}} -> "Sähköpostiosoite puuttuu."
      {:email, {:too_long, _}} -> "Sähköpostiosoite on liian pitkä."
      {:email, :format} -> "Sähköpostiosoite on väärää muotoa."
      _ -> "???"
    end

    conn = Flash.put conn, :error, msg
    handle_register_errors conn, rest
  end

  defp handle_register_errors(conn, []), do: conn
end


