defmodule Rekkaritarkkailu.User do
  use Ecto.Model
  alias Rekkaritarkkailu.Repo

  # Before creating, set the joined_at timestamp to current time
  def set_joined_time(changeset) do
    change changeset, %{joined_at: Ecto.DateTime.utc}
  end

  before_insert :set_joined_time

  schema "users" do
    field :username, :string
    field :email, :string
    field :hash, :string
    field :recovery_hash, :string
    field :joined_at, Ecto.DateTime
  end

  def changeset(user, :create, params) do
    user
    |> cast(params, ~w(email username hash), ~w())
    |> validate_length(:email, min: 1)
    |> validate_length(:email, max: 255)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:hash, max: 130)
    |> validate_length(:username, min: 1)
    |> validate_length(:username, max: 50)
    |> validate_unique(:username, on: Repo)
  end
end