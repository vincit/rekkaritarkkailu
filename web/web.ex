defmodule Rekkaritarkkailu.Web do
  @moduledoc """
  A module that keeps using definitions for controllers,
  views and so on.

  This can be used in your application as:

      use Rekkaritarkkailu.Web, :controller
      use Rekkaritarkkailu.Web, :view

  Keep the definitions in this module short and clean,
  mostly focused on imports, uses and aliases.
  """

  def view do
    quote do
      use Phoenix.View, root: "web/templates"

      # Import URL helpers from the router
      import Rekkaritarkkailu.Router.Helpers

      # Import all HTML functions (forms, tags, etc)
      use Phoenix.HTML

      # Import flash messaging
      alias Rekkaritarkkailu.Flash

      # Import authentication status functions
      import Rekkaritarkkailu.AuthLib, only: [current_user: 1, is_authed: 1]
    end
  end

  def controller do
    quote do
      use Phoenix.Controller

      # Alias the data repository as a convenience
      alias Rekkaritarkkailu.Repo

      # Import URL helpers from the router
      import Rekkaritarkkailu.Router.Helpers
    end
  end

  def model do
    quote do
      use Ecto.Model
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
