defmodule Rekkaritarkkailu.Flash do
  import Phoenix.Controller, only: [put_flash: 3, get_flash: 2]

  @moduledoc """
  An implementation of flash messages that support multiple messages per key.

  The default Phoenix flash system only allows one value per key. This module
  inserts a list into that key, thus allowing the insert of many values for the
  same key.
  """

  def put(conn, key, value) do
    new_val = case get(conn, key) do
      old_vals when is_list(old_vals) -> [value | old_vals]
      old_val -> [value | [old_val]]
    end

    put_flash conn, key, new_val
  end

  def get(conn, key) do
    case get_flash conn, key do
      list when is_list(list) -> list
      nil -> []
      val -> [val]
    end
  end
end